'use strict';

const fs = require('fs');
require('dotenv').config();
const path = require('path');
const { readFileSync } = require('fs');
const { should, expect } = require('chai');
const createClient = require('../lib/client');

const invoice = readFileSync(path.join(__dirname, 'xml', 'signed.xml'), { encoding: 'utf-8' });

let client = null;

describe('Module: Document', () => {
  before(done => {
    createClient(process.env.GRPC_HOST, process.env.GRPC_PORT, 'InvoiceService')
      .then(_client => {
        client = _client;
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('Method: Build document (210 mm)', done => {
    client.document(
      {
        invoice,
        cic: '100',
        csc: '0123456789',
        host: 'http://hnfce.fazenda.mg.gov.br/portalnfce',
        width: 210,
        columnSpacing: 1
      },
      (err, res) => {
        should().not.exist(err);
        expect(res.pdf).to.be.instanceOf(Buffer);
        fs.writeFileSync(path.join(__dirname, 'out', '210mm.pdf'), res.pdf);
        done();
      }
    );
  });

  it('Method: Build document (80 mm)', done => {
    client.document(
      {
        invoice,
        cic: '100',
        csc: '0123456789',
        host: 'http://hnfce.fazenda.mg.gov.br/portalnfce',
        width: 80,
        columnSpacing: 1
      },
      (err, res) => {
        should().not.exist(err);
        expect(res.pdf).to.be.instanceOf(Buffer);
        fs.writeFileSync(path.join(__dirname, 'out', '80mm.pdf'), res.pdf);
        done();
      }
    );
  });

  it('Method: Build document (48 mm)', done => {
    client.document(
      {
        invoice,
        cic: '100',
        csc: '0123456789',
        host: 'http://hnfce.fazenda.mg.gov.br/portalnfce',
        width: 80,
        columnSpacing: 1
      },
      (err, res) => {
        should().not.exist(err);
        expect(res.pdf).to.be.instanceOf(Buffer);
        fs.writeFileSync(path.join(__dirname, 'out', '48mm.pdf'), res.pdf);
        done();
      }
    );
  });
});
