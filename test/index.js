'use strict';

describe('Module: DANFE', () => {
  before(function(done) {
    this.timeout(5000);
    // Starts the service
    require('../index');
    done();
  });

  // Document tests
  require('./document');
  // QR Code tests
  require('./qrCode');
});
