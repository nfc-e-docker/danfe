'use strict';

const { expect } = require('chai');
const { qrCode } = require('../lib');

describe('Module: QR Code', () => {
  it('Method: URL (homologation)', done => {
    qrCode
      .url({
        host: 'http://hnfce.fazenda.mg.gov.br/portalnfce',
        key: '31180871984409000138650650000000011065000018',
        version: 2,
        day: 31,
        total: 9999.99,
        digestValue: '9f76945aa196ef8e4c6a3610497b438972adb67e497b438972adb67e',
        cic: '000000',
        csc: '0123456789',
        env: 2
      })
      .then(url => {
        expect(url).to.be.equal(
          'http://hnfce.fazenda.mg.gov.br/portalnfce?p=31180871984409000138650650000000011065000018|2|2|31|9999.99|9f76945aa196ef8e4c6a3610497b438972adb67e497b438972adb67e|000000|b559fc3a2e2f16dbf4dfe3ba06b95f166ee1ee40'
        );
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('Method: URL (contigency)', done => {
    qrCode
      .url({
        host: 'http://hnfce.fazenda.mg.gov.br/portalnfce',
        key: '31180871984409000138650650000000011065000018',
        version: 2,
        day: 31,
        total: 9999.99,
        digestValue: '9f76945aa196ef8e4c6a3610497b438972adb67e497b438972adb67e',
        cic: '000000',
        csc: '0123456789',
        env: 1
      })
      .then(url => {
        expect(url).to.be.equal(
          'http://hnfce.fazenda.mg.gov.br/portalnfce?p=31180871984409000138650650000000011065000018|2|1|31|9999.99|9f76945aa196ef8e4c6a3610497b438972adb67e497b438972adb67e|000000|efe802d3e652165395a135c53387a28207c6ad7b'
        );
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('Method: QR Code (homologation)', done => {
    qrCode
      .image(
        'http://hnfce.fazenda.mg.gov.br/portalnfce?p=31180871984409000138650650000000011065000018|2|1|000000|f99de6684d41f36b131957e494d5556782b32409'
      )
      .then(svg => {
        expect(svg).to.be.contain('svg');
        done();
      })
      .catch(err => {
        done(err);
      });
  });
});
