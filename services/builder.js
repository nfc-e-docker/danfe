'use strict';

const { parseString } = require('xml2js');
const { document, qrCode } = require('../lib');
const { formatDate } = require('../lib');

module.exports = args => {
  const { invoice, width, columnSpacing = 1, cic, csc, host } = args;
  return new Promise((resolve, reject) => {
    // Object parser (from xml to obj)
    parseString(invoice, { attrkey: 'attrkey', explicitArray: false, normalizeTags: false }, async (err, parsed) => {
      try {
        if (err) throw new Error({ message: 'Document parser has been chashed', err });
        const obj = parsed;
        // Issuer/recipient address and additional info.
        const { emit: issuer, dest: recipient, infAdic: additionalInfo, ide: invoiceInfo } = obj.nfeProc.NFe.infNFe;

        // Cash info (savings, raw value, interest, etc)
        const values = obj.nfeProc.NFe.infNFe.total.ICMSTot;
        // Payment details
        const paymentDetails = obj.nfeProc.NFe.infNFe.pag.detPag;
        // Invoice key.
        const key = obj.nfeProc.NFe.infNFe.attrkey.Id.replace('NFe', '');
        // Invoice issue date.
        const issueDate = new Date(new Date(invoiceInfo.dhEmi) - 180 * 60 * 1000);

        // Pameters related to authorization info.
        const authorizationInfo = obj.nfeProc.protNFe.infProt;

        // Products
        const products = Array.isArray(obj.nfeProc.NFe.infNFe.det)
          ? obj.nfeProc.NFe.infNFe.det
          : [obj.nfeProc.NFe.infNFe.det];

        const qrCodeUrl = await qrCode.url({
          host,
          env: 2,
          key,
          version: 2,
          day: formatDate(new Date(issueDate)).substring(0, 2),
          total: values.vNF,
          digestValue: Buffer.from(obj.nfeProc.NFe.Signature.SignedInfo.Reference.DigestValue).toString('hex'),
          cic,
          csc
        });
        const qrCodeSvg = await qrCode.image(qrCodeUrl);

        const pdf = await document({
          issuer,
          recipient,
          additionalInfo,
          values,
          paymentDetails,
          key,
          issueDate,
          authorizationInfo,
          width,
          columnSpacing,
          products,
          qrCodeSvg,
          invoiceInfo
        });
        resolve(pdf);
      } catch (err) {
        reject({ message: 'Builder has been failed', err });
      }
    });
  });
};
