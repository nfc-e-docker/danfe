'use strict';

/**
 * @description Formats the date to dd/mm/yyyy hh:mm:ss pattern.
 * @param {Date} date Date-time to format.
 * @returns {String} Date-time in the following format: dd/mm/yyyy hh:mm:ss.
 */
module.exports = date =>
  `${date
    .toISOString()
    .substring(0, 10)
    .split('-')
    .reverse()
    .join('-')
    .replace(/-/g, '/')} ${date.toLocaleTimeString('pt-BR')}`;
