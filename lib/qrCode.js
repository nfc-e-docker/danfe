'use strict';

const QRCode = require('qrcode');
const crypto = require('crypto');

const hash = value =>
  crypto
    .createHash('sha1')
    .update(value)
    .digest('hex');

const length = value => new TextEncoder('utf-8').encode(value).length;

/**
 * @description Generates the query url for QR code.
 * @param {Object} args Data needed to generate the query url.
 * @returns {String} Query url.
 */
exports.url = args => {
  return new Promise((resolve, reject) => {
    const { host, env, key, version, day, total, digestValue, cic, csc } = args;
    if (length(key) !== 44) reject('Invalid passkey size');
    if (length(day) !== 2) reject('Invalid day size');
    if (length(digestValue) !== 56) reject('Invalid digest value size');
    if (length(cic) < 1 || length(cic) > 6) reject('Invalid cic size');
    if (length(version) !== 1) reject('Invalid version size');

    let params = `${key}|${version ? version : 2}|${env}|`;
    params += day && total && digestValue ? `${day}|${total}|${digestValue}|${cic}` : `${cic}`;
    // Finally, sha-1 algorithm is applied over all parameters to generate the query url.
    const url = hash(`${params}${csc}`);
    resolve(`${host}?p=${params}|${url}`);
  });
};

/**
 * @description Generates a graphical representation of a QR code (SVG format).
 * @param {String} url QR Code content.
 * @returns {SVGElement} QR Code as SVG element.
 */
exports.image = url =>
  new Promise((resolve, reject) => {
    QRCode.toString(url, { type: 'svg' }, (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });
