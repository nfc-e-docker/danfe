'use strict';

const PDFDocument = require('pdfkit');
const concat = require('concat-stream');
const templates = require('../templates');
const { global, paymentOptions, url, font } = require('../static');
const formatDate = require('./formatDate');
const SVGtoPDF = require('svg-to-pdfkit');

PDFDocument.prototype.svg = function(svg, x, y, options) {
  return SVGtoPDF(this, svg, x, y, options), this;
};

module.exports = args => {
  return new Promise((resolve, reject) => {
    const {
      issuer,
      recipient,
      additionalInfo,
      values,
      paymentDetails,
      key,
      issueDate,
      authorizationInfo,
      width,
      columnSpacing,
      products,
      qrCodeSvg,
      invoiceInfo
    } = args;
    // Document style
    const layout = templates[width] || templates[210];
    const doc = new PDFDocument({ size: layout.size, margins: layout.margins });
    doc.fontSize(layout.fontSize);
    doc.font(layout.font);

    // Nested object to flat object transformation.
    const flat = {
      ...issuer.enderEmit,
      CNPJ: issuer.CNPJ,
      xNome: issuer.xNome,
      xFant: issuer.xFant,
      IE: issuer.IE,
      IM: issuer.IM
    };

    // Document fill.
    const documentHeader = Object.keys(flat).reduce((prevVal, currVal) => {
      return prevVal.replace(new RegExp(`{{${currVal}}}`, 'g'), flat[currVal] || '');
    }, global().documentHeader.text);
    // Invoice header.
    doc.font(font.courierBold);
    doc.text(documentHeader, global().documentHeader.config);
    // Restores default font.
    doc.font(font.courier);
    doc.moveDown(1);
    doc.text(global(columnSpacing).tableHeader.text);

    // Products
    products.forEach(product => {
      doc.text(
        `${product.prod.cProd} ${product.prod.xProd} ${product.prod.qCom} ${product.prod.uCom} ${product.prod.vUnCom} ${product.prod.vProd}\n\n`
      );
    });
    doc.moveDown(1);

    doc.text(global().totalItems.text, global().totalItems.config);
    doc.text(`${products.length || 1}`, { align: 'right' });
    doc.text(global().totalValue.text, global().totalValue.config);
    // we can use reduce to improve that.
    doc.text(`${values.vProd}`, { align: 'right' });
    doc.text(global().discount.text, global().discount.config);
    doc.text(`${values.vDesc}`, { align: 'right' });
    doc.text(global().finalValue.text, global().finalValue.config);
    doc.text(`${values.vNF}`, { align: 'right' });
    doc.moveDown();
    doc.text(global().paymentMethod.text, global().paymentMethod.config);
    doc.text(global().receivedValue.text, global().receivedValue.config);
    doc.text(`${paymentOptions[paymentDetails.tPag]}`, { continued: true });
    doc.text(`${paymentDetails.vPag}`, { align: 'right' });
    doc.text(global().change.text, global().change.config);
    doc.text(`${paymentDetails.vTroco || '0.00'}`, { align: 'right' });
    doc.text(global().acessLink.text, global().acessLink.config);
    doc.moveDown(1);
    doc.text(
      `${
        invoiceInfo.tpAmb === '1' ? url.production[invoiceInfo.cUF] : url.homologation[invoiceInfo.cUF]
      }\n${key.replace(/(\d{4})/g, '$1 ')}`,
      {
        align: 'center'
      }
    );
    doc.moveDown(1);
    // Left QR Code - only for larger formats like A4 or Letter.
    if (width === 210) {
      doc.svg(qrCodeSvg, doc.x, doc.y, { width: layout.qrCodeSize, height: layout.qrCodeSize });
      doc.x = layout.xCurssor;
    }
    doc.moveDown(1);
    // Checks if exist a identified consumer.
    if (recipient) {
      doc.text(`CONSUMIDOR ${recipient.CPF ? 'CPF ' : 'CNPJ '}`, { continued: true });
      doc.text(`${recipient.CPF || recipient.CNPJ}`);
    } else {
      doc.text(global().noRecipient.text);
    }
    doc.moveDown(1);

    doc.text(
      global()
        .invoiceNumber.text.replace('{{nNF}}', invoiceInfo.nNF.padStart(9, '0'))
        .replace('{{serie}}', invoiceInfo.serie.padStart(3, '0')),
      global().invoiceNumber.config
    );
    doc.text(formatDate(issueDate));
    doc.moveDown(1);
    doc.text(
      global()
        .authorizationInfo.text.replace('{{nProt}}', authorizationInfo.nProt)
        .replace('{{dhRecbto}}', formatDate(new Date(authorizationInfo.dhRecbto)))
    );

    // Centralized QR Code - only for small paper formats.
    if (width < 210) {
      doc.svg(qrCodeSvg, ((width - (layout.margins.left * 25.4) / 72) / 2 - 15) * 2.83, doc.y, {
        width: layout.qrCodeSize,
        height: layout.qrCodeSize
      });
      doc.y += layout.qrCodeSize;
    }
    doc.moveDown(3);
    doc.text(`${additionalInfo ? additionalInfo.infCpl : ''}`);

    doc.pipe(
      concat(res => {
        if (!res) reject('PDF cannot be null');
        resolve(res);
      })
    );
    doc.end();
  });
};
