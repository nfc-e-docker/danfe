'use strict';

module.exports = {
  qrCode: require('./qrCode'),
  formatDate: require('./formatDate'),
  document: require('./document'),
  certs: require('./certs'),
  timeout: require('./timeout')
};
