'use strict';
module.exports = {
  global: require('./global'),
  paymentOptions: require('./paymentOptions'),
  url: require('./url'),
  font: require('./font')
};
