'use strict';

/**
 * @descriprtion Stores invoice url for consultation according its UF.
 */

module.exports = {
  production: Object.freeze({
    21: 'nfce.sefaz.ma.gov.br/portal/consultarNFCe.jsp',
    31: 'https://nfce.fazenda.mg.gov.br/portalnfce/sistema/qrcode.xhtml'
  }),
  homologation: Object.freeze({
    21: 'homologacao.sefaz.ma.gov.br/portal/consultarNFCe.jsp',
    31: 'https://nfce.fazenda.mg.gov.br/portalnfce/sistema/qrcode.xhtml'
  })
};
