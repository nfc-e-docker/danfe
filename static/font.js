'use strict';

module.exports = {
  courier: 'Courier',
  courierBold: 'Courier-Bold'
};
