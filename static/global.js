'use strict';

module.exports = spacing => ({
  tableHeader: {
    text: `Código ${' '.repeat(spacing)}Descrição${' '.repeat(spacing)}Qtde${' '.repeat(spacing)}UN${' '.repeat(
      spacing
    )}Vl Unit${' '.repeat(spacing)}Vl Total${' '.repeat(spacing)}`
  },
  documentHeader: {
    text: `CNPJ {{CNPJ}} {{xNome}}
  {{xLgr}}, {{nro}}, {{xBairro}}, {{xMun}}, {{UF}}
  Documento Auxiliar de Nota Fiscal Eletrônica`,
    config: { align: 'center' }
  },
  totalItems: { text: 'Qtde. total de itens', config: { continued: true } },
  totalValue: { text: 'Valor total R$', config: { continued: true } },
  discount: { text: 'Desconto R$', config: { continued: true } },
  finalValue: { text: 'Valor a pagar R$', config: { continued: true } },
  paymentMethod: { text: 'FORMA DE PAGAMENTO', config: { continued: true } },
  receivedValue: { text: 'VALOR PAGO', config: { align: 'right' } },
  change: { text: 'Troco R$', config: { continued: true } },
  acessLink: { text: '\nConsulte pela Chave de Acesso em\n', config: { align: 'center' } },
  recipient: { text: 'CONSUMIDOR ', config: { continued: true } },
  noRecipient: { text: 'CONSUMIDOR NÃO IDENTIFICADO', config: {} },
  invoiceNumber: { text: 'NFC-e nº {{nNF}} Série {{serie}} ', config: { continued: true } },
  authorizationInfo: {
    text: 'Protocolo de autorização: {{nProt}}\nData de autorização: {{dhRecbto}}',
    config: {}
  }
});
