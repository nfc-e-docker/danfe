'use strict';

require('dotenv').config();
const grpc = require('grpc');
const path = require('path');
const { certs, timeout } = require('./lib');
const { builder } = require('./services');
const protoLoader = require('@grpc/proto-loader');

module.exports.init = async () => {
  // Proto loading
  const packageDefinition = await protoLoader.load(path.join(__dirname, 'protos', 'danfe.proto'));
  const proto = grpc.loadPackageDefinition(packageDefinition);

  const server = new grpc.Server();

  server.addService(proto.InvoiceService.service, {
    echo: (call, callback) => {
      callback(null, 'pong');
    },
    document: async (call, callback) => {
      try {
        const { invoice, columnSpacing, width, cic, csc, host } = call.request;
        const pdf = await builder({ invoice, columnSpacing, width, cic, csc, host });
        callback(null, { pdf });
      } catch (err) {
        callback(err);
      }
    }
  });

  server.bind(
    `${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`,
    grpc.ServerCredentials.createSsl(certs.rootCerts, certs.keyCertPairs, true)
  );
  server.start();

  if (process.env.TEST_TYPE === 'integrated') {
    // Registers at gateway.
    timeout.connectToGateway((err, _res) => {
      if (err) {
        // Gracefully shuts down the server. The server will stop receiving new calls, and any pending calls will complete.
        server.tryShutdown(() => {
          process.exit(1);
        });
      }
      // Checks gateway heartbeat.
      timeout.heartbeat();
    });
  }
};
