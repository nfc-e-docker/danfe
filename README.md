# Danfe

O DANFE  (Documento Auxiliar da Nota Fiscal Eletrônica) representa a concretização de uma operação fiscal de consumidor. O presente serviço é responsável pela emissão do arquivo de DANFE em formato PDF de acordo com os padrões 210 mm , 80 mm e 58 mm.