'use strict';

module.exports = {
  size: [595.28, 841.89],
  margins: {
    top: 72,
    bottom: 72,
    left: 30,
    right: 30
  },
  fontSize: 10,
  font: 'Courier',
  qrCodeSize: 85.04,
  xCurssor: 120
};
