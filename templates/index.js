'use strict';

module.exports = {
  210: require('./210'),
  80: require('./80'),
  48: require('./48')
};
