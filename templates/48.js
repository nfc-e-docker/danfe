'use strict';

module.exports = {
  size: [136.06, 841.89],
  margins: {
    top: 5.67,
    bottom: 5.67,
    left: 5.67,
    right: 5.67
  },
  fontSize: 10,
  font: 'Courier',
  qrCodeSize: 85.04
};
